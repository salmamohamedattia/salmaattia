package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class LinearHashing<K,V> implements IHashLinearProbing , IHash<K,V> {
	private int capacity = 1200;
	private HashNode<K,V>[]hashTable = new HashNode[capacity];
	private ArrayList<K> keys = new ArrayList<K>();
	private ArrayList<V> values = new ArrayList<V>();
	private int size = 0 ;
	private int collisions = 0 ;
	public LinearHashing() {
		intialize();
	}
	@Override
	public void put(K key, V value) {
			HashNode<K,V> node = new HashNode<K,V>();
			keys.add(key);
			values.add(value);
			node.setKey(key);
			node.setValue(value);
			node.setDeleted(false);
		    hashTable[getHashIndex(key)] = node ; 
		    size ++;
		    if(size  == capacity){
		    	resize(capacity * 2);
		    }
	}
	@Override
	public String get(K key) {
		HashNode<K,V> node = hashTable[getHashIndex(key)];
		if(node != null){
			if(node.getKey() == null && node.isDeleted()){
				int i = 1;
				int hash = (((key.hashCode()) % (hashTable.length)) + i) % (hashTable.length);
				i++;
				while (hashTable[hash] != null && !(hashTable[hash].getKey().equals(key))){
					hash = (((key.hashCode()) % (hashTable.length)) + i) % (hashTable.length);
					i++;
				}
				return (String) hashTable[hash].getValue();
			}else {
				return (String) node.getValue();
			}
		}
		return null;
	}
	@Override
	public void delete(K key) {
		HashNode<K,V> node = hashTable[getHashIndex(key)];
		if(node != null && node.getKey().equals(key)){
			node.setKey(null);
			node.setDeleted(true);
			keys.remove(key);
			values.remove(node.getValue());
			size -- ;
		}
	}

	@Override
	public boolean contains(K key) {
		HashNode<K,V> node = hashTable[getHashIndex(key)];
		if(node != null && node.getKey().equals(key)){
			return true ;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return (size == 0);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public int collisions() {
		return collisions;
	}
	@Override
	public Iterable<K> keys() {
		return keys;
	}
	private void intialize(){
		for(int i = 0 ; i < hashTable.length ; i++){
			hashTable[i]= null;
		}
	}
	private int getHashIndex(K key){
		int i = 1;
		boolean probed = false ;
		int hash = (key.hashCode()) % (hashTable.length);
		while (hashTable[hash] != null && !(hashTable[hash].getKey().equals(key))){
			collisions ++ ;
			hash = (((key.hashCode()) % (hashTable.length)) + i) % (hashTable.length);
			i++;
			probed = true ;
		}
		if(probed){
			collisions ++ ;
		}
		return hash;	
	}
	private void rehashing(){
		collisions = collisions + hashTable.length + 1 ;
		hashTable = new HashNode[capacity];
		intialize();
		for(int i = 0;i<keys.size();i++){//rehashing
			HashNode<K,V> node = new HashNode<K,V>();
			node.setKey(keys.get(i));
			node.setValue(values.get(i));
		    hashTable[getHashIndex(keys.get(i))] = node ;
		}
	}
	private void resize(int capacity){
		this.capacity = capacity ;
		rehashing();
	}
}
