package eg.edu.alexu.csd.filestructure.hash;

public class HashNode<K,V> {
	private V value ;
	private K key ;
	private boolean deleted = false;
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}
	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
}
