package eg.edu.alexu.csd.filestructure.hash;
import java.util.ArrayList;
import java.util.LinkedList;

public class Chaining<K,V> implements IHashChaining , IHash<K,V>{
	private LinkedList<HashNode<K,V>>[]hashTable = new LinkedList[1200];
	private ArrayList<K> keys = new ArrayList<K>();
	private int size = 0 ;
	private int collision = 0 ;
	public Chaining() {
		for(int i = 0 ; i < hashTable.length ; i++){
			LinkedList<HashNode<K,V>> list = new LinkedList<HashNode<K,V>>();
			hashTable[i]=list ;
		}
	}
	private int getHashIndex(K key){
		return ((key.hashCode()) % (hashTable.length));
	}
	@Override
	public void put(K key, V value) {
		HashNode<K,V> node = new HashNode<K,V>();
		node.setKey(key);
		node.setValue(value);
		keys.add(key);
		if(hashTable[getHashIndex(key)] != null){
			collision = collision + hashTable[getHashIndex(key)].size();
		}
		hashTable[getHashIndex(key)].add(node);
		size++;
	}
	@Override
	public String get(K key) {
		LinkedList<HashNode<K,V>> list =hashTable[getHashIndex(key)];
		for(int i = 0 ; i<list.size() ; i ++){
			if(key.equals(list.get(i).getKey())){
				return (String) list.get(i).getValue();
			}
		}
		return null;
	}

	@Override
	public void delete(K key) {
		LinkedList<HashNode<K,V>> list =hashTable[getHashIndex(key)];
		for(int i = 0 ; i<list.size() ; i ++){
			if(key.equals(list.get(i).getKey())){
				list.remove(i);
				keys.remove(key);
				size -- ;
			}
		}
	}

	@Override
	public boolean contains(K key) {
		if(get(key)== null){
			return false;
		}
		return true;
	}

	@Override
	public boolean isEmpty() {
		return (size == 0);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int capacity() {
		return 1200;
	}

	@Override
	public int collisions() {
		return collision;
	}

	@Override
	public Iterable<K> keys() {
		return keys;
	}
	public void printTable(){
		for(int i = 0 ; i < hashTable.length ; i++){
			for(int j = 0 ; j < hashTable[i].size();j++){
				System.out.print(hashTable[i].get(j).getValue()+",");
			}
			System.out.println();
		}
	}
}
