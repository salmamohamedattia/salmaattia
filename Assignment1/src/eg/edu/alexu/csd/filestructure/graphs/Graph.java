package eg.edu.alexu.csd.filestructure.graphs;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.management.RuntimeErrorException;

public class Graph implements IGraph{

	private ArrayList<Integer> vertices = new ArrayList<Integer>();
	private int numOfVertices = 0 ;
	private int numOfEdges = 0 ;
	private ArrayList<ArrayList<Integer>> adjList = new ArrayList<ArrayList<Integer>>();
	private int[][] weight ;
	private ArrayList<Integer> shPath = new ArrayList<Integer>();
	@Override
	public void readGraph(File file) {
		FileReader filereader ;
		int count = 0 ;
		try {
			filereader = new FileReader(file.getAbsolutePath());
			BufferedReader reader = new BufferedReader(filereader);
			String word = reader.readLine();
			while (word != null) {
				int[]param = split(word);
				if(param.length == 2){
					if(count != 0){
						throw new RuntimeErrorException(null);
					}
					 numOfVertices = param[0];
					 numOfEdges = param[1];
					 if(numOfEdges > ((numOfVertices)*(numOfVertices - 1))){
						 throw new RuntimeErrorException(null);
					 }
					 weight = new int [numOfVertices][numOfVertices];
					 for(int index = 0 ; index < numOfVertices ; index++){
						 ArrayList<Integer> neighbour = new ArrayList<Integer>();
						 adjList.add(neighbour);
					 }
				}else if(param.length == 3 && count >= 0){
					count++ ;
					if((param[0] >= 0 && param[0] < numOfVertices) && (param[1] >= 0 && param[1] < numOfVertices)){
						(adjList.get(param[0])).add(param[1]);
						weight[param[0]][param[1]] = param[2];
					}else{
						throw new RuntimeErrorException(null);
					}
				}else{
					throw new RuntimeErrorException(null);
				}
				if(count > numOfEdges){
					throw new RuntimeErrorException(null);
				}
				word = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			throw new RuntimeErrorException(null);
		} catch (IOException e) {
			throw new RuntimeErrorException(null);
		}
		
	}
	@Override
	public int size() {
		return numOfEdges;
	}
	@Override
	public ArrayList<Integer> getVertices() {
		ArrayList<Integer> vertices = new ArrayList<Integer>();
		for(int i = 0 ; i < numOfVertices ; i++){
			vertices.add(i);
		}
		return vertices;
	}
	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		return adjList.get(v);
	}
	@Override
	public void runDijkstra(int src, int[] distances) {
		boolean[] sptSet = new boolean[numOfVertices];
		for (int i = 0; i < numOfVertices; i++)
        {
            distances[i] = (Integer.MAX_VALUE)/2;
            sptSet[i] = false;
        }
		distances[src] = 0;
		for(int i = 0 ; i < numOfVertices-1 ; i++){
			int minDist = minDistance(distances, sptSet);
			sptSet[minDist] = true;
			shPath.add(minDist);
			for(int j = 0 ; j < numOfVertices ; j++){
				 if (!sptSet[j] && weight[minDist][j]!=0 && distances[minDist] != Integer.MAX_VALUE && distances[minDist]+ weight[minDist][j] < distances[j]){
	                    distances[j] = distances[minDist] + weight[minDist][j];
				 }
			}
		}
	}
	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		return shPath;
	}
	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		for (int i=0; i<numOfVertices; ++i){
            distances[i] = (Integer.MAX_VALUE)/2;
		}   
        distances[src] = 0;
        for(int a = 1 ; a < numOfVertices  ; a++){
        for (int i=0; i<numOfVertices; i++)
        {
            for (int j=0; j<getNeighbors(i).size(); j++)
            {
                int u = i;
                int v = getNeighbors(i).get(j);
                int weightOfEdge = weight[u][v];
                if (distances[u]!=(Integer.MAX_VALUE)/2 &&
                    distances[u]+weightOfEdge<distances[v])
                    distances[v]=distances[u]+weightOfEdge;
            }
        }
        }
        for (int i=0; i<numOfVertices; i++)
        {
            for (int j=0; j<getNeighbors(i).size(); j++)
            {
                int u = i;
                int v = getNeighbors(i).get(j);
                int weightOfEdge = weight[u][v];
                if (distances[u]!=(Integer.MAX_VALUE)/2 && distances[u]+weightOfEdge<distances[v])
                    return false ;
            }
        }
		return true;
	}
	private int[] split (String word){
		String[] param = word.split(" ");
		int[] result = new int[param.length];
		for(int i = 0 ; i < param.length ; i++){
			result[i]=Integer.parseInt(param[i]);
		}
		return result ;
	}
	private int minDistance(int[] distances , boolean[] sptSet){
		int min = Integer.MAX_VALUE;
		int min_index = -1;
		for (int v = 0; v < numOfVertices; v++)
            if (sptSet[v] == false && distances[v] <= min)
            {
                min = distances[v];
                min_index = v;
            }
 
        return min_index;
	}
}
