package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {
	private INode<T> leftChild = null;
	private INode<T> rightChild = null;
	private INode<T> parent = null;
	private T value = null;
	private int height = 1;

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public INode<T> getLeftChild() {

		return leftChild;
	}

	@Override
	public INode<T> getRightChild() {

		return rightChild;
	}

	@Override
	public T getValue() {

		return value;
	}

	@Override
	public void setValue(T value) {

		this.value = value;
	}

	public void setLeftChild(INode<T> leftChild) {
		this.leftChild = leftChild;
	}

	public void setRightChild(INode<T> rightChild) {
		this.rightChild = rightChild;
	}

	public void setParent(INode<T> parent) {
		this.parent = parent;
	}

	public INode<T> getParent() {
		return parent;
	}

}
