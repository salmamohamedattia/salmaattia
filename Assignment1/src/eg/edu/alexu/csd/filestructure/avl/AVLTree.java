package eg.edu.alexu.csd.filestructure.avl;

public class AVLTree<T extends Comparable<T>> implements IAVLTree<T> {
private INode<T> root = null;
private String error = "";
private int i = 0;

@Override
public void insert(T key) {
	error += "insert : " + key;
	if (key != null) {
		root = insertRecursion(root, key);
	}
}

@Override
public boolean delete(T key) {

	if( ! search(key)){
		return false ;
	}
	else{
		root = deleteRecursion(this.root, key);
		return true ;
	}
	}

	@Override
	public boolean search(T key) {
		INode<T> test = this.searchRecursion(this.root, key);
		if (test == null) {
			return false;
		}
		return true;
	}

	@Override
public int height() {
		return this.heightOfNode(root);
	}

	@Override
public INode<T> getTree() {
		return root;
	}

private INode<T> insertRecursion(INode<T> node, T key) {
		// 1)insertion
		if (node == null) { // set the root
			INode<T> node2 = new Node<T>();
			node2.setValue(key);
			return node2;
		}
		if (key.compareTo(node.getValue()) <= 0) {

			((Node<T>) node).setLeftChild(insertRecursion(node.getLeftChild(), key));
			INode<T> left = node.getLeftChild();
			((Node<T>) left).setParent(node);

		} else {

			((Node<T>) node).setRightChild(insertRecursion(node.getRightChild(), key));
			INode<T> right = node.getRightChild();
			((Node<T>) right).setParent(node);
		}
		// 2)update the height
		if (node.getRightChild() != null && node.getLeftChild() != null) {
			((Node<T>) node).setHeight(
					max(((Node<T>) node.getLeftChild()).getHeight(), ((Node<T>) node.getRightChild()).getHeight()) + 1);
		} else if (node.getLeftChild() == null) {
			((Node<T>) node).setHeight(heightOfNode(node.getRightChild()) + 1);
		} else {
			((Node<T>) node).setHeight(heightOfNode(node.getLeftChild()) + 1);
		}
		// 3)get balance factor to check whether it is balanced or not
		int balanceFactor = getBalance(node);
		return rebalance(balanceFactor, key, node);
	}

private INode<T> rebalance(int balanceFactor, T key, INode<T> node) {
		if (balanceFactor > 1 && key.compareTo(node.getLeftChild().getValue()) <= 0) {
			return rightRotate(node);
		}
		if (balanceFactor < -1 && key.compareTo(node.getRightChild().getValue()) > 0) {
			return leftRotate(node);
		}
		if (balanceFactor > 1 && key.compareTo(node.getLeftChild().getValue()) >= 0) {
			((Node<T>) node).setLeftChild(leftRotate(node.getLeftChild()));
			return rightRotate(node);
		}
		if (balanceFactor < -1 && key.compareTo(node.getRightChild().getValue()) < 0) {
			((Node<T>) node).setRightChild(rightRotate(node.getRightChild()));
			return leftRotate(node);
		}
		return node;
	}

	// calculate the maximum of 2 comparable
private int max(int a, int b) {
		if (a > b) {
			return a;
		}
		return b;
	}

	// calculate the balance factor
private int getBalance(INode<T> node) {
		if (node == null || (node.getLeftChild() == null && node.getRightChild() == null)) {
			return 0;
		}
		if (node.getLeftChild() != null && node.getRightChild() != null) {
			return (((Node<T>) node.getLeftChild()).getHeight()) - (((Node<T>) node.getRightChild()).getHeight());
		} else if (node.getRightChild() == null) {
			return ((Node<T>) node.getLeftChild()).getHeight();
		}
		return ((((Node<T>) node.getRightChild()).getHeight())) * -1;
	}

	// perform right rotation

private INode<T> rightRotate(INode<T> node) {
		INode<T> parentNode = ((Node<T>) node).getParent();
		INode<T> left = ((Node<T>) node).getLeftChild();
		if (parentNode != null) {
			if (parentNode.getLeftChild() != null
					&& node.getValue().compareTo(parentNode.getLeftChild().getValue()) == 0) {
				((Node<T>) parentNode).setLeftChild(left);
			} else {
				((Node<T>) parentNode).setRightChild(left);
			}
		} else {
			root = left;
		}
		((Node<T>) left).setParent(parentNode);
		((Node<T>) node).setLeftChild(left.getRightChild());
		if (left.getRightChild() != null) {
			((Node<T>) left.getRightChild()).setParent(node);
		}
		((Node<T>) left).setRightChild(node);
		((Node<T>) node).setParent(left);

		((Node<T>) node)
				.setHeight(max(this.heightOfNode(node.getLeftChild()), this.heightOfNode(node.getRightChild())) + 1);
		((Node<T>) left)
				.setHeight(max(this.heightOfNode(left.getLeftChild()), this.heightOfNode(left.getRightChild())) + 1);
		return (left);

	}
	// perform left rotation

	// gets the height of any node in the tree
private int heightOfNode(INode<T> node) {
		if (node == null) {
			return 0;
		}
		return ((Node<T>) node).getHeight();
	}

private INode<T> leftRotate(INode<T> node) {
		INode<T> parentNode = ((Node<T>) node).getParent();
		INode<T> right = node.getRightChild();
		if (parentNode != null) {
			if (parentNode.getLeftChild() != null
					&& parentNode.getLeftChild().getValue().compareTo(node.getValue()) == 0) {
				((Node<T>) parentNode).setLeftChild(right);

			} else {
				((Node<T>) parentNode).setRightChild(right);
			}
		} else {
			root = right;
		}
		((Node<T>) right).setParent(parentNode);
		((Node<T>) node).setRightChild(right.getLeftChild());
		if (right.getLeftChild() != null) {
			((Node<T>) right.getLeftChild()).setParent(node);
		}
		((Node<T>) right).setLeftChild(node);
		((Node<T>) node).setParent(right);
		((Node<T>) node)
				.setHeight(max(this.heightOfNode(node.getLeftChild()), this.heightOfNode(node.getRightChild())) + 1);
		((Node<T>) right)
				.setHeight(max(this.heightOfNode(right.getLeftChild()), this.heightOfNode(right.getRightChild())) + 1);
		return (right);

	}

private INode<T> deleteRecursion(INode<T> node, T key) {
		if (node == null) {
			return node;
		}
		if (key.compareTo(node.getValue()) < 0) {
			INode<T> check = deleteRecursion(node.getLeftChild(), key);
			((Node<T>) node).setLeftChild(check);
			if (check != null) {
				((Node<T>) check).setParent(node);
			}
		} else if (key.compareTo(node.getValue()) > 0) {
			INode<T> check = deleteRecursion(node.getRightChild(), key);
			((Node<T>) node).setRightChild(check);
			if (check != null) {
				((Node<T>) check).setParent(node);
			}
		} else {
			if (node.getLeftChild() == null || node.getRightChild() == null) {
				INode<T> temp = null;
				if (temp == node.getLeftChild()) {
					temp = node.getRightChild();
				} else {
					temp = node.getLeftChild();
				}
				if (temp == null) {
					temp = node;
					node = null;
				} else {
					node = temp ;
				}
			} else {
				INode<T> temp = successor(node.getRightChild());
				node.setValue(temp.getValue());
				INode<T> check = this.deleteRecursion(node.getRightChild(), temp.getValue());
				((Node<T>) node).setRightChild(check);
				if(check != null){
					((Node<T>) check).setParent(node);
				}
			}
		}
		if (node == null) {
			return node;
		}
		((Node<T>) node)
				.setHeight(this.max(heightOfNode(node.getLeftChild()), this.heightOfNode(node.getRightChild())) + 1);
		int balance = getBalance(node);
		if (balance > 1 && getBalance(node.getLeftChild()) >= 0) {
			return rightRotate(node);
		}
		if (balance > 1 && getBalance(node.getLeftChild()) < 0) {
			INode<T> check = leftRotate(node.getLeftChild());
			((Node<T>) node).setLeftChild(check);
			if(check != null){
				((Node<T>) check).setParent(node);
			}
			return rightRotate(node);
		}
		if (balance < -1 && getBalance(node.getRightChild()) <= 0) {
			return leftRotate(node);
		}
		if (balance < -1 && getBalance(node.getRightChild()) > 0) {
			INode<T> check = rightRotate(node.getRightChild());
			((Node<T>) node).setRightChild(check);
			if(check != null){
				((Node<T>) check).setParent(node);
			}
			return leftRotate(node);
		}
		return node;
	}

private INode<T> successor(INode<T> node) {
		INode<T> current = node;
		while (current.getLeftChild() != null) {
			current = current.getLeftChild();
		}
		return current;
	}

private INode<T> searchRecursion(INode<T> node, T key) {
		if (node == null) {
			return node;
		} else if (key == null) {
			return null;
		} else if (node.getValue().compareTo(key) == 0) {
			return node;
		} else if (node.getValue().compareTo(key) > 0) {
			return searchRecursion(node.getLeftChild(), key);
		} else {
			return searchRecursion(node.getRightChild(), key);
		}
	}

}
