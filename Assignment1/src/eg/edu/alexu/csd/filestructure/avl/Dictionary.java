package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary {
	private int size = 0;
	private IAVLTree<String> tree = new AVLTree<String>();

	@Override
	public void load(File file) {
		FileReader filereader;
		try {
			filereader = new FileReader(file.getAbsolutePath());
			BufferedReader reader = new BufferedReader(filereader);
			String word = reader.readLine();
			while (word != null) {
				insert(word);
				word = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean insert(String word) {
		if (exists(word)) {
			return false;
		}
		tree.insert(word);
		size++;
		return true;
	}

	@Override
	public boolean exists(String word) {
		return tree.search(word);
	}

	@Override
	public boolean delete(String word) {
		if (exists(word)) {
			tree.delete(word);
			size--;
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int height() {
		return tree.height();
	}

}
