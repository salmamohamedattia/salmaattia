package eg.edu.alexu.csd.filestructure.sort;

import java.awt.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class Heap <T extends  Comparable<T>> implements IHeap<T>{
	private java.util.List<INode<T>> heapList = new LinkedList<INode<T>>();
	@Override
	public INode<T> getRoot() {
		return heapList.get(0);
	}
	@Override
	public int size() {
		return heapList.size() ;
	}
	@Override
	public void heapify(INode<T> node) {
		INode<T> leftNode = node.getLeftChild(); 
		INode<T> rightNode = node.getRightChild();
		boolean flag = false ;
		boolean rightCheck = false ;
		T nodeValue = node.getValue();
		T largest = null ;
		if( leftNode != null && nodeValue.compareTo((leftNode).getValue()) < 0){
			largest = (leftNode).getValue() ;
		}
		else{
			flag = true ;
			largest = nodeValue ;
		}
		if(rightNode != null && largest.compareTo((rightNode).getValue()) < 0){
			largest = (rightNode).getValue() ;
			rightCheck = true ;
			flag = false ;
		}
		if(!flag){
			if(!rightCheck){
				leftNode.setValue(nodeValue);
				node.setValue(largest);
				this.heapify(leftNode);
			}else{
				rightNode.setValue(nodeValue);
				node.setValue(largest);
				this.heapify(rightNode);
			}
		}
	}
	@Override
	public T extract() {
		if(heapList.size() > 1){
			T max = heapList.get(0).getValue();
			T min = heapList.get(heapList.size()-1).getValue();
			heapList.get(0).setValue(min);
			if(min.compareTo(heapList.get(heapList.size()-1).getParent().getLeftChild().getValue()) == 0){
				((Node<T>) heapList.get(heapList.size()-1).getParent()).setLeftChild(null) ;
				//((Node<T>) heapList.get(heapList.size()-1)).setParent(null);
			}else{
				((Node<T>) heapList.get(heapList.size()-1).getParent()).setRightChild(null) ;
				//((Node<T>) heapList.get(heapList.size()-1)).setParent(null);
			}
			heapList.remove(heapList.size()-1);
			this.heapify(heapList.get(0));
            return max ;
		}else if(heapList.size() == 1 ){
			T max = heapList.get(0).getValue();
			heapList.remove(0);
			return max ;
		}
		return null;
	}
	@Override
	public void insert(T element) {
		INode<T> key = new Node<T>() ;
		heapList.add( key);
		int i1 = heapList.size() - 1 ;		
		int parentIndex= 0 ;
		key.setValue(element);
		if(i1 % 2 == 0){
			 parentIndex =  ((i1)/2)-1 ;
		}else{
			 parentIndex = ((i1 +1)/2)-1;
		}
		if(heapList.size() > 1 ){
			INode<T> parent = heapList.get(parentIndex);
			((Node<T>)key).setParent(parent);
			this.insertHelp(key, parent);
		}
			int i = heapList.size() - 1 ;
			while( i > 0 && (heapList.get(i).getParent().getValue()).compareTo(heapList.get(i).getValue())<0){
				T temp = heapList.get(i).getValue();
				heapList.get(i).setValue((heapList.get(i).getParent().getValue()));
				heapList.get(i).getParent().setValue(temp);
				if(i % 2 == 0){
					 i =  ((i)/2)-1 ;
				}else{
					 i = ((i+1)/2)-1;
				}
			}
		
	}
	@Override
	public void build(Collection<T> unordered) {
		heapList = new LinkedList<INode<T>>();
		Iterator<T> itr = unordered.iterator();
		if(itr.hasNext()){
			heapList.add(new Node<T>());
			heapList.get(0).setValue(itr.next());
			while(itr.hasNext()){
				T element = itr.next();
				this.insert(element);
			}
		}
		
	}
	private void insertHelp(INode<T> node , INode<T> parent){
		
		if(parent.getLeftChild() == null){
			((Node<T>)parent).setLeftChild(node);
		}else if(parent.getRightChild() == null){
			((Node<T>)parent).setRightChild(node);
		}
	}
	public void printHeap (){
		for(int i = 0 ; i < heapList.size() ; i++){
			System.out.println(heapList.get(i).getValue().toString());
		}
	}
	public void sortingHeap (){
		int i;
		T temp;
		int n = (heapList.size());
		  for (i = (n / 2) - 1; i >= 0; i--){
		    this.siftDown( n,i);
		  }
		  for (i = n-1 ; i >= 1; i--)
		  {
		    temp = heapList.get(0).getValue();
		    heapList.get(0).setValue(heapList.get(i).getValue());
		    heapList.get(i).setValue(temp);
		    this.siftDown(i , 0);
		  }
	}
	private void siftDown( int n, int i)
	{
		int largest = i;  // Initialize largest as root
        int l = 2*i + 1;  // left = 2*i + 1
        int r = 2*i + 2;  // right = 2*i + 2
        if (l < n && heapList.get(largest).getValue().compareTo(heapList.get(l).getValue())<0)
            largest = l ;
        if (r < n && heapList.get(largest).getValue().compareTo(heapList.get(r).getValue())<0)
            largest = r;
        if (largest != i)
        {
        	T temp = heapList.get(i).getValue();
		    heapList.get(i).setValue(heapList.get(largest).getValue());
		    heapList.get(largest).setValue(temp);
            siftDown( n, largest);
        }
	}
}
