package eg.edu.alexu.csd.filestructure.sort;

public class Node <T extends  Comparable<T>> implements INode<T> {
	private INode<T> leftChild  ;
	private INode<T> rightChild  ;
	private INode<T> parent  ;
	private T value  ;
	
	//-----------------------------------------------setters
	public void setLeftChild(INode leftChild){
		this.leftChild = leftChild ;
	}
	public void setRightChild(INode rightChild){
		this.rightChild = rightChild ;
	}
	public void setParent(INode parentChild){
		this.parent = parentChild ;
	}
	@Override
	public void setValue(T value) {
		this.value = value ;
	}
	//----------------------------------------------getters
	@Override
	public INode getLeftChild() {
		return leftChild;
	}

	@Override
	public INode getRightChild() {
		return rightChild;
	}

	@Override
	public INode getParent() {
		return parent;
	}

	@Override
	public T getValue() {
		return value;
	}

	
}
