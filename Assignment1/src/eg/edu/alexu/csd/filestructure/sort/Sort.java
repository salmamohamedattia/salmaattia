package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class Sort <T extends  Comparable<T>> implements ISort<T>{

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		IHeap<T> heap = new Heap<T>();
		heap.build(unordered);
		((Heap<T>) heap).printHeap();
		System.out.println("------------------ sorted :");
		
		for(int i = unordered.size()-1  ; i >= 0 ; i-- ){
			((Heap<T>) heap).sortingHeap();
		}
		((Heap<T>) heap).printHeap();
		return heap;
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		for(int i = 0 ; i < unordered.size() ; i++){
			for(int j = 0 ; j < unordered.size() - 1 ; j++){
				if((unordered.get(j)).compareTo(unordered.get(j+1)) > 0){
					T temp = unordered.get(j);
					unordered.set(j , unordered.get(j+1) );
					unordered.set(j+1 , temp);
				}
			}
		}
	}

	@Override
	public void sortFast(ArrayList<T> unordered) {
		this.quickSort(unordered, 0, unordered.size()-1);
		
	}
	public void quickSort (ArrayList<T> unordered , int p , int r){
		if(p < r){
			T x =  unordered.get(r);
            int  i = p - 1 ;
            for(int j = p ; j < r  ; j++){
            	if(unordered.get(j).compareTo(x) <= 0){
            		i++;
            		T temp = unordered.get(i);
            		unordered.set(i,unordered.get(j));
            		unordered.set(j, temp);
            	}
            }
            T temp = unordered.get(i+1);
    		unordered.set(i+1,unordered.get(r));
    		unordered.set(r, temp);
			int q = i+1 ;
			quickSort(unordered,p,q-1);
			quickSort(unordered,q+1,r);
		}
	}

}
